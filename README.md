Features
==========
* Set column number and enable/disable scroll bar；
* Add header view；
* View pager；
* Set <code>onClickListener</code> and <code>onScrollListener</code> to item；
* Methods depends on activity life circle: <code>onActivityPause()</code>、<code>onActivityResume()</code>、<code>onActivityDestroy()</code>；
* Set policy of adding items：<code>DEFAULT</code>(left to right, top to bottom)、<code>SHORTEST_COLUMN_FIRST</code>；
* Pull-to-refresh and auto-loading-more;
* Preloading depends on scroll direction：

Todo
==========
* Implementing <code>DataSetObserver</code>;

Usage
==========
* This project references <code>PullToRefresh</code>, <code>RLAsyncTask</code> and some classes in [Roid-Library](https://github.com/RincLiu/Roid-Library). So you should add it as a Library Project.
* If you've called <code>setItemOrder(ItemOrder.SHORTEST_COLUMN_FIRST)</code>，a <code>LayoutParams</code> should be set to the item view，especially the height:

```java
...
wfv.setItemOrder(ItemOrder.SHORTEST_COLUMN_FIRST);
wfv.setWaterfallItemHandler(new WaterfallItemHandler(){
	@Override
	public View onCreateItemView(int position) {
		//TODO: Simulating the process of creating item view
		ImageView iv=new ImageView(this);
		iv.setScaleType(ScaleType.FIT_XY);
		//If you called setItemOrder(ItemOrder.SHORTEST_COLUMN_FIRST),
		//you should set layout parameters to the item, especially the height.
		iv.setLayoutParams(
			new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 100)
		);
		return iv;
	}
	...
});
...
```
